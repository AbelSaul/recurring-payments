module.exports = {
	find: function(req,res){
		User.find({sort:"createdAt DESC"})
				.then((users)=>{
					return res.view("users/index",{users:users})
				})
				.catch((err)=>{
					console.log(err)
					return res.redirect("/");
				})
	},
	admin: function(req,res){
		// /admin/1
		// req.body.admin 
		// 1 => administrador
		// 0 => no administrador
		User.findOne({id: req.params.id}).then((user)=>{
			user.admin = req.body.admin == "1";
			user.save().then(()=> res.redirect("/user"));
		})
		.catch((err)=>{
				console.log(err)
				return res.redirect("/");
			})
		
	}
}