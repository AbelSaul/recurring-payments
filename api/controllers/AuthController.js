/**
 * AuthController
 *
 * @description :: Server-side logic for managing Auths
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

let passport = require('passport');

module.exports = {
	show:function(req,res){
		res.view('auth/show');
	},
	create: function(req,res){
		// Creando la sesión
		passport.authenticate('local',function(err,user,extraInfo){
			console.log(extraInfo);
			console.log(err);
			if(err || !user)
				return res.view('auth/show',{});

			req.login(user,function(err){
				if(err)
					return res.view('auth/show',{error: err});

				return res.view('homepage',{user:user})

			})

		})(req,res);


	},
	destroy: function(req,res){
		
		if(typeof req.session.passport != "undefined"){

			req.session.passport.user = null;
		}
		console.log(req.session.passport)

		res.redirect("/");
	}
};

