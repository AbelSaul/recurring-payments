
let stripe = require('stripe')('sk_test_EzEtezZSFf2wSjXEdK7qKiyP');

module.exports = {
	attributes:{
		user:{
			model: "user"
		},
		plan:{
			model: "plan"
		},
		status:{
			type: "string",
			defaultsTo: "inactive"
		},
		nextBilling:{
			type: "date"
		},
		stripe_id:{
			type: "string"
		}
	},
	updateFromRemote: function(stripeId){
		console.log(stripeId);
		return new Promise(function(resolve,reject){
			stripe.subscriptions.retrieve(stripeId,function(err,sripeSubscription){
				if(err) return reject(err);
				
				console.log(sripeSubscription.status)

				let updateSubPromise = Subscription.update({stripe_id: stripeId},{

					status: sripeSubscription.status
				}).then(resolve).catch(reject);

			})
		})
	},
	createAndStripe: function(opts){
		// Crear una suscripción en Stripe
		// Crear la suscripción dentro de nuestra BD
		// User / Plan => {user: null, plan: null}

		return new Promise(function(resolve,reject){

			stripe.subscriptions.create({
				customer: opts.user.customer_id,
				plan: opts.plan.id
			},function(err,stripeSubscription){
				
				if(err){
					reject(err)
				}else{
					Subscription.create({
						plan: opts.plan,
						user: opts.user,
						stripe_id: stripeSubscription.id
					})
					.then(sub => resolve(sub))
					.catch(err => reject(err));
				
				}


			})

		});

	}
}