let stripe = require('stripe')('sk_test_EzEtezZSFf2wSjXEdK7qKiyP');

module.exports = {
	attributes:{
		name:{
			type: 'string',
			required: true
		},
		amount:{
			type: 'integer',
			required: true
		},
		interval:{
			type: 'string',
			required: true
		},
		stripe_valid:{
			type: 'boolean',
			defaultsTo: false
		}
	},
	afterCreate:function(plan,callback){
		stripe.plans.create({
			amount: plan.amount,
			interval: plan.interval,
			name: plan.name,
			id: plan.id,
			currency: 'mxn'
		},function(err,stripe_plan){
			if(err){
				console.log(err);
				callback();
			}else{
				Plan.update({id: plan.id},{stripe_valid: true})
						.exec(callback)
			}
		});
	},
	afterDestroy:function(plansDeleted,callback){
		stripe.plans.del(plansDeleted[0].id,function(err,confirmation){
			if(err) console.log(err);

			callback();
		});
	}
};

